var Account = require('../models/Account');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
module.exports = {
    getAll: function(req,res){
    var query = { user_id: new ObjectId(req.user._id) };
    console.log(query);
		Account.find(query, function(err,accounts){
			if (err) res.send(err);
			console.log(accounts);
			console.log(err);
      res.json(accounts);
		});
	},
	getOne: function(req,res){
	  var query = { _id: req.params.account_id, user_id: req.user._id };
		Account.findOne(query, function(err,account){
			if (err)
				res.send(err);

				res.json(account);
		});
	},
	put: function(req,res){
		var query = { _id: req.params.account_id, user_id: req.user._id };
		Account.findOneAndUpdate(query, req.body, function(err,account){
			if (err)
				res.send(err);

				res.json({ message: 'Account updated.' });
		});
	},
	post: function(req,res){
	  var account = new Account(req.body);
    account.user_id = req.user._id;
		account.save(function(err, account){
		    if (err){
			    res.send(err);
			}
			
			res.json({ message: 'Account created: ' + account._id });
		});
	},
	delete: function(req,res){
		var query = { _id: req.params.account_id, user_id: req.user._id };
		Account.findOneAndRemove(query, function(err,account){
			if (err)
				res.send(err);

				res.json({ message: 'Account removed.' });
		});
	},
}