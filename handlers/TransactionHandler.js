var Account = require('../models/Account');
var Transaction = require('../models/Transaction');

module.exports = {
    getAll: function(req,res){
	    var query = { _id: req.params.account_id };
		Account.findOne(query, function(err,account){
			if (err) res.send(err);
			
			res.json(account.transactions);
		});
	},
	getOne: function(req,res){
		var query = { _id: req.params.account_id };
		Account.findOne(query, function(err,account){
			if (err) res.send(err);
			
			res.json(account.transactions.id(req.params.tran_id));
		});
	},
	put: function(req,res){
		var query = { _id: req.params.account_id };
		Account.findOne(query, function(err,account){
			if (err) res.send(err);
			var tran = account.transactions.id(req.params.tran_id);
			
			tran.amount = req.body.amount;			
			tran.description = req.body.description;
			tran.tran_date = req.body.tran_date;
			
			account.save(function(err){
				if (err) res.send(err);
				res.json({ message: 'Transaction updated.' });
			});
		});
	},
	post: function(req,res){
	    var query = { _id: req.params.account_id };
		Account.findOne(query, function(err,account){
			if (err) res.send(err);
			
			var transaction = new Transaction(req.body);
			account.transactions.push(transaction);
			
			account.save(function(err, account){
				if (err) res.send(err);
				
				res.json({ message: 'Transaction created.' });
			});
		});
	},
	delete: function(req,res){
		var query = { _id: req.params.account_id };
		Account.findOne(query, function(err,account){
			if (err) res.send(err);
			account.transactions.id(req.params.tran_id).remove(function(err){
				if (err) {
          res.send(err);
        }
        account.save(function(err,account){
          if(err) res.send(err);
          res.json({ message: 'Transaction removed.' });
        });
				
			});
		});
	},
}