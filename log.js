var scribe = require('scribe');

module.exports = function(app) {
  // Configuration
  // --------------
  scribe.configure(function(){
    scribe.set('app', 'Crunchr.io');
    scribe.set('logPath', './logs');
    scribe.set('defaultTag', 'General');
    scribe.set('divider', ':::');
    scribe.set('identation', 5);
    scribe.set('maxTagLength', 30);
    scribe.set('mainUser', 'root');
  });
  
  // Create Loggers
  // --------------
  scribe.addLogger("log", true , true, 'green');            // (name, save to file, print to console, tag color)
  scribe.addLogger('realtime', true, true, 'underline'); 
  scribe.addLogger('high', true, true, 'magenta');
  scribe.addLogger('normal', true, true, 'white');
  scribe.addLogger('low', true, true, 'grey');
  scribe.addLogger('info', true, true, 'cyan');
  
  // Express.js
  app.use(scribe.express.logger(function(req, res){
    return false;
  }));
  
  app.get('/log', scribe.express.controlPanel());
};