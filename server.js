// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express'); 		// call express
var app        = express(); 				// define our app using express
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var AccountHandler = require('./handlers/AccountHandler');
var TransactionHandler = require('./handlers/TransactionHandler');
var path = require("path");
var apiRouter = require('./routers/ApiRouter');
var viewRouter = require('./routers/ViewRouter');
var session = require('express-session');
var os = require('os');
var passport = require('passport')
  , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var User = require('./models/User.js');
var config = require('./config');

config = config || { };


console.log('Starting up...');
console.log('I am: ' + os.hostname());

app.use(cookieParser());
app.use(bodyParser());

app.use(session({secret: config.secret || 'test', saveUninitialized: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());

var realm = (config.scheme || 'http://') + (config.hostName || 'localhost') + '/';
var returnUrl = realm + 'auth/google/return';

passport.use(
  new GoogleStrategy(
    {
      clientID: config.GOOGLE_CLIENT_ID || '',
      clientSecret: config.GOOGLE_CLIENT_SECRET || '',
      callbackURL: returnUrl,
      scope: [
        'https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile'
      ]
    },
    function(accessToken, refreshToken, profile, done) {
      
      User.findOne({ openId: profile.id }, function(err, user) {
        if (err) {
          return done(err);
        }
        
        if (!user){
          user = new User({
            provider: profile.provider,
            openId: profile.id,
            displayName: profile.displayName,
            email: profile.emails[0].value,
            firstName: profile.name.givenName,
            lastName: profile.name.familyName,
            photoUrl: profile._json.picture
          });
         
          user.save(function(err){
            if (err) console.log(err);
            return done(err,user);
          });
        }
        
        done(err, user);
      });
    }
  ));

passport.serializeUser(function(user, done) {
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  User.findOne({ _id: id }, function(err, user) {
    done(err, user);
  });
});

app.get('/auth/google', passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/userinfo.profile',
                                            'https://www.googleapis.com/auth/userinfo.email'] }));
app.get('/auth/google/return', passport.authenticate('google', { successRedirect: '/', failureRedirect: '/login' }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

console.log('setting up static routes...');

// public folders
app.use('/assets', express.static(__dirname + '/assets'));
console.log('setting up routers...');

apiRouter.setup(express, app, { account: AccountHandler, transaction: TransactionHandler });
viewRouter.setup(express, app);

console.log('firing up the server');

// START THE SERVER
// =============================================================================
var port = config.port || 8080; 		// set our port
app.listen(port);
console.log('server running on port ' + port);