appRoot.controller('AccountsController', ['$scope', '$resource', '$location', 'models', function ($scope, $resource, $location, models) {
  $scope.accounts = [];
  $scope.newAccount = [];
  
  
  
  Array.prototype.sum = function(property){
    return this.reduce(function(t,c,i,a) { return t + c[property]; }, 0);
  }
  $scope.sum_transactions = function(account) {
    if (!account.transactions) return 0;
    else return account.transactions.sum('amount');
  };
  
  $scope.getAccounts = function () {
      return models.api.Account.query().then(function (data) {
          $scope.accounts = data;
      });
  }
  
  $scope.createAccount = function(){
    $scope.newAccount.save().$promise.then(function(){
      init();
    });
  }
  
  $scope.month_name = new Date().toLocaleString("en-us", {month: "long"});
  
  var init = function() {
    $scope.newAccount = new models.api.Account();
    $scope.getAccounts().then(function(){
      if ($scope.accounts.length == 0) {
        $location.path('/intro');
      }
    });
  };
  init();
}]);