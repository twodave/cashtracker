appRoot.controller('AccountController', ['$scope', '$resource', '$routeParams', '$location', 'models', function ($scope, $resource, $routeParams, $location, models) {
  $scope.accounts = [];
  Array.prototype.sum = function(property){
    return this.reduce(function(t,c,i,a) { return t + c[property]; }, 0);
  }
  $scope.sum_transactions = function(account){
    if (!account.transactions) return 0;
    else return account.transactions.sum('amount');
  };
  
  $scope.account = [];
  $scope.newTransaction = new models.api.Transaction();
  
  $scope.getAccounts = function () {
      return models.api.Account.query().then(function (data) {
          $scope.accounts = data;
          var matches = $.grep($scope.accounts, function(account) {
            return account._id == $routeParams.account_id
          });
          if (!matches.length){
            $location.path('/');
          }else{
            $scope.account = matches[0];
            $scope.transactions = $scope.account.transactions;
          }
      });
  }
  $scope.transactions = [];
  
  $scope.filterOptions = {
    filterText: ''
  };
  $scope.transactionGrid = {
    plugins: [new ngGridFlexibleHeightPlugin()],
    data: 'transactions',
    multiselect: false,
    enableRowSelection: false,
    enableColumnResize: true,
    showColumnMenu: true,
    filterOptions: $scope.filterOptions,
    columnDefs: [
      { groupable: false, sortable: false, cellTemplate: '<input type="button" style="margin-left:5px;margin-top: 2px;" ng-confirm-click="Are you sure?" ng-click="destroyTransaction(row.entity);" value="Delete" />', width: '*' },
      { field: 'tran_date', cellTemplate: '<div class="ngCellText ng-scope col1 colt1" ng-class="col.colIndex()">{{ row.getProperty(col.field) | date: "MM-dd-yyyy" }}</div>', displayName: 'Date', width: '***' },
      { field: 'description', displayName: 'Description', width: '*******' },
      { field: 'category', displayName: 'Category', width: '****' },
      { field: 'amount', cellTemplate: '<div class="ngCellText ng-scope col4 colt4" ng-class="col.colIndex()">{{ row.getProperty(col.field) | currency }}</div>', displayName: 'Amount', width: '***' }
    ]
  };
  
  $scope.insertTransaction = function(){
    //date hack
    $scope.newTransaction.tran_date = new Date($scope.newTransaction.tran_date);
    $scope.newTransaction.save($scope.account).$promise.then(function(){
      $scope.newTransaction = new models.api.Transaction();
      $scope.getAccounts();
    });
  }
  
  $scope.destroyTransaction = function(data) {
    var transaction = new models.api.Transaction(data);
    transaction.destroy($scope.account).$promise.then(function(){
      $scope.getAccounts();
    });
  };
  
  $scope.month_name = new Date().toLocaleString("en-us", {month: "long"});
  function ngGridFlexibleHeightPlugin (opts) {
    var self = this;
    self.grid = null;
    self.scope = null;
    self.init = function (scope, grid, services) {
        self.domUtilityService = services.DomUtilityService;
        self.grid = grid;
        self.scope = scope;
        var recalcHeightForData = function () { setTimeout(innerRecalcForData, 1); };
        var innerRecalcForData = function () {
            var gridId = self.grid.gridId;
            var footerPanelSel = '.' + gridId + ' .ngFooterPanel';
            var extraHeight = self.grid.$topPanel.height() + $(footerPanelSel).height();
            var naturalHeight = self.grid.$canvas.height() + 1;
            if (opts != null) {
                if (opts.minHeight != null && (naturalHeight + extraHeight) < opts.minHeight) {
                    naturalHeight = opts.minHeight - extraHeight - 2;
                }
            }

            var newViewportHeight = naturalHeight + 3;
            if (!self.scope.baseViewportHeight || self.scope.baseViewportHeight !== newViewportHeight) {
                self.grid.$viewport.css('height', newViewportHeight + 'px');
                self.grid.$root.css('height', (newViewportHeight + extraHeight) + 'px');
                self.scope.baseViewportHeight = newViewportHeight;
                self.domUtilityService.RebuildGrid(self.scope, self.grid);
            }
        };
        self.scope.catHashKeys = function () {
            var hash = '',
                idx;
            for (idx in self.scope.renderedRows) {
                hash += self.scope.renderedRows[idx].$$hashKey;
            }
            return hash;
        };
        self.scope.$watch('catHashKeys()', innerRecalcForData);
        self.scope.$watch(self.grid.config.data, recalcHeightForData);
    };
}
  var init = function() {
    $scope.getAccounts().then(function(){
      if ($scope.accounts.length == 0) {
        $location.path('/intro');
      }
    });
  };
  init();
}]);