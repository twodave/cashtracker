appRoot.controller('IntroController', ['$scope', '$resource', '$rootScope', '$timeout', '$location', 'models', function ($scope, $resource, $rootScope, $timeout, $location, models) {
  $scope.newAccount = [];
  
  $scope.createAccount = function(){
    $scope.newAccount.save().$promise.then(function(){
      $location.path('/');
    });
  }
  
  var init = function() {
    $scope.newAccount = new models.api.Account();
  }
  init();
}]);