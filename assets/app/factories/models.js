﻿var models = angular.module('ctModels', []).
  factory('models', ['apiService', '$filter', function (apiService,$filter) {
      
      var models = {};

      models.api = {};

      // begin Example
      models.api.Example = function (serverExample) {
          this.exampleID = 0;
          this.message = "Hello World!";

          angular.extend(this, serverExample);
      }

      models.api.Example.path = function () { return "/api/examples/:id"; }

      models.api.Example.query = function (params) { return apiService.query(models.api.Example.path(), models.api.Example, params); }

      models.api.Example.prototype.save = function (callback) {
          if (this.exampleID) {
              return apiService.update(models.api.Example.path(), this.exampleID, this);
          } else {
              return apiService.insert(models.api.Example.path(), this);
          }
      };
      
      models.api.Example.prototype.destroy = function (callback) { return apiService.destroy(models.api.Example.path(), this.exampleID); };
      // end Example
      
      // begin Account
      models.api.Account = function (serverAccount) {
          this.display_name = "";
          this.last_updated = new Date();
          this.transactions = [];
          this.user_id = null;
          
          angular.extend(this, serverAccount);
      }

      models.api.Account.path = function () { return "/api/accounts/:account_id"; }

      models.api.Account.query = function (params) { return apiService.query(models.api.Account.path(), models.api.Account, params); }

      models.api.Account.prototype.save = function (callback) {
          if (this._id) {
              return apiService.update(models.api.Account.path(), this._id, this);
          } else {
              return apiService.insert(models.api.Account.path(), this);
          }
      };
      
      models.api.Account.prototype.destroy = function (callback) { return apiService.destroy(models.api.Account.path(), this._id); };
      // end Account
      
      // begin Transaction
      models.api.Transaction = function (serverTransaction) {
          this.description = "";
          this.category = "";
          this.tran_date = null;
          this.amount = null;
          angular.extend(this, serverTransaction);
      }

      models.api.Transaction.path = function () { return "/api/accounts/:account_id/transactions/:transaction_id"; }

      models.api.Transaction.query = function (params) { return apiService.query(models.api.Transaction.path(), models.api.Transaction, params); }

      models.api.Transaction.prototype.save = function (account) {
          if (this._id) {
              return apiService.update(models.api.Transaction.path(), {account_id: account._id, transaction_id: this._id}, this);
          } else {
              return apiService.insert(models.api.Transaction.path(), this, {account_id: account._id});
          }
      };
      
      models.api.Transaction.prototype.destroy = function (account) { return apiService.destroy(models.api.Transaction.path(), {account_id: account._id, transaction_id: this._id}); };
      // end Transaction

      return models;
  }]);