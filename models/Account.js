var mongoose = require('../db/connection');
var Transaction = require('./Transaction');

var AccountSchema = new mongoose.Schema({
  display_name: String,
  last_updated: { type: Date, default:Date.now },
  user_id: mongoose.Schema.Types.ObjectId,
  transactions: [Transaction.schema]
});
var Account = mongoose.model('Account', AccountSchema);

module.exports = Account;
module.exports.schema = AccountSchema;