var mongoose = require('../db/connection');

var UserSchema = new mongoose.Schema({
  provider: String,
  openId: String,
  displayName: String,
  firstName: String,
  lastName: String,
  email: String,
  photoUrl: String
});
var User = mongoose.model('User', UserSchema);

module.exports = User;
module.exports.schema = UserSchema;