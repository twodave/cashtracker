var mongoose = require('../db/connection');

var TransactionSchema = new mongoose.Schema({
  description: String,
  category: String,
  tran_date: { type: Date, default: Date.now },
  amount: Number
});

var Transaction = mongoose.model('Transaction', TransactionSchema);

module.exports = Transaction;
module.exports.schema = TransactionSchema;