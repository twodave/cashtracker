# CashTracker #

CashTracker is a simple, smart personal finance app that anyone can use.

### Software Stack ###

* MongoDB
* NodeJS w/ Express & Mongoose
* AngularJS

### Trello ###

* Check out the [board](https://trello.com/b/ldy5npAS)

### Getting Started ###

* **Full config.json example:** 
```
#!javascript
{ 
  "mongoString": "localhost:27017", 
  "sessionSecret": "abc123",
  "port": 8080,
  "hostName": "example.com",
  "scheme": "http://",
  "GOOGLE_CLIENT_ID": "my-client-id", // you can set these up at:
  "GOOGLE_CLIENT_SECRET": "my-secret" // https://console.developers.google.com/
}
```

To start the app, run: `node server`. Then navigate to hostName:port.

* **Coming Soon:** API documentation

### Contribution guidelines ###

1. Create a topic branch
2. Push your changes into the topic branch
3. Submit a pull request to have your changes merged back into master

### Who do I talk to? ###

* Email Dave: twodave@gmail.com