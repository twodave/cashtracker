module.exports = {
  setup: function(express, app, handlers){
    var apiRouter = express.Router();
    
    // middleware to use for all requests
    apiRouter.use(function(req, res, next) {
      // do logging
      if (req.user) {
        if (req.body.length > 0){
          console.log(JSON.stringify(req.body));
        }
        console.log('[api] ' + req.method + ': ' + req.url);
        next(); // make sure we go to the next routes and don't stop here
      } else {
        res.send(403,'Not Authorized');
      }
    });

    // test route to make sure everything is working (accessed at GET http://localhost:8080/api)
    apiRouter.get('/', function(req, res) {
      res.render('index',{title:'API Info'});
    });

    apiRouter.route('/accounts')
      .get(handlers.account.getAll)
      .post(handlers.account.post);

    apiRouter.route('/accounts/:account_id')
    .get(handlers.account.getOne)
    .put(handlers.account.put)
    .delete(handlers.account.delete);

    apiRouter.route('/accounts/:account_id/transactions')
    .post(handlers.transaction.post)
    .get(handlers.transaction.getAll);

    apiRouter.route('/accounts/:account_id/transactions/:tran_id')
    .get(handlers.transaction.getOne)
    .put(handlers.transaction.put)
    .delete(handlers.transaction.delete);


    app.use('/api', apiRouter);
  }
}