module.exports = {
  setup: function(express, app){

    var viewRouter = express.Router();
    
    viewRouter.use(function(req, res, next) {
      if (!req.user && req.url != '/login') {
        res.redirect('/login');
        res.end();
      }
      else{
        next();
      }
    });
    
    viewRouter.get('/', function(req, res) {
      res.render('index',{title:'Cash Tracker', user: req.user});
    });
    viewRouter.get('/login', function(req, res) {
      res.render('login',{title:'Cash Tracker'});
    });
    viewRouter.get('/partials/:name', function(req, res) {
      res.render('partials/' + req.params.name,{ });
    });
    
		app.use('/', viewRouter);
	}
}